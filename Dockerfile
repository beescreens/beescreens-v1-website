FROM nginx:latest

# Copy sources
COPY . /usr/share/nginx/html

# Expose port
EXPOSE 80

# Start the server
CMD ["nginx", "-g", "daemon off;"]